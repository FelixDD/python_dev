#Ce code complémente la classe Particule
import numpy as np
import matplotlib.pyplot as plt
def distribution_vitesse(nombre_particules, energie_cinetique):
    i = False
    somme_x = 0
    somme_y = 0
    while i == False:
        vitesse_x = np.random.uniform(1, 10000, nombre_particules)
        vitesse_y = np.random.uniform(1, 10000, nombre_particules)
        for j in range(0, nombre_particules):
            somme_x = somme_x + (vitesse_x[j])**2
            somme_y = somme_y + (vitesse_y[j])**2
        energie_supposer = np.sqrt(somme_x+somme_y)
        if energie_cinetique - 1000 <= energie_supposer <= energie_cinetique + 1000:
            i = True
    return vitesse_x, vitesse_y
