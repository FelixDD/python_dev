import numpy as np
import random
import matplotlib.pyplot as plt
# Cette fonction donne une matrice 2x4 donnant les coordonnées des 4 sites pour une molécule
# d'eau 3D. L est la longueur du carré.
def creation_mol_eau(L):
    direction = random.randint(1,4)
    angle_H1 = (2*np.pi/360)*104.52*np.random.random(1)
    angle_H2 = (2*np.pi/360)*104.52 - angle_H1
    pos_O = L*np.random.rand(2,1)
    pos_H1 = np.zeros(2)
    pos_H2 = np.zeros(2)
    pos_polair = np.zeros(2)
    if direction == 1:
        pos_H1[0] = pos_O[0] + np.sin(angle_H1) * 0.09572
        pos_H1[1] = pos_O[1] + np.cos(angle_H1) * 0.09572
        pos_H2[0] = pos_O[0] - np.sin(angle_H2) * 0.09572
        pos_H2[1] = pos_O[1] + np.cos(angle_H2) * 0.09572
    if direction == 2:
        pos_H1[0] = pos_O[0] + np.cos(angle_H1) * 0.09572
        pos_H1[1] = pos_O[1] + np.sin(angle_H1) * 0.09572
        pos_H2[0] = pos_O[0] + np.cos(angle_H2) * 0.09572
        pos_H2[1] = pos_O[1] - np.sin(angle_H2) * 0.09572
    if direction ==3:
        pos_H1[0] = pos_O[0] - np.sin(angle_H1) * 0.09572
        pos_H1[1] = pos_O[1] - np.cos(angle_H1) * 0.09572
        pos_H2[0] = pos_O[0] + np.sin(angle_H2) * 0.09572
        pos_H2[1] = pos_O[1] - np.cos(angle_H2) * 0.09572
    if direction ==4:
        pos_H1[0] = pos_O[0] - np.cos(angle_H1) * 0.09572
        pos_H1[1] = pos_O[1] - np.sin(angle_H1) * 0.09572
        pos_H2[0] = pos_O[0] - np.cos(angle_H2) * 0.09572
        pos_H2[1] = pos_O[1] + np.sin(angle_H2) * 0.09572
    coordonneex = [pos_O[0], pos_H1[0], pos_H2[0]]
    coordonneey = [pos_O[1], pos_H1[1], pos_H2[1]]
    return coordonneex, coordonneey
coordonneex, coordonneey = creation_mol_eau(5)
plt.scatter(coordonneex, coordonneey)
plt.show()
print(np.arctan(np.abs((coordonneex[0]-coordonneex[1])/(coordonneey[0]-coordonneey[1])))+np.arctan(np.abs((coordonneex[0]-coordonneex[2])/(coordonneey[0]-coordonneey[2]))))

