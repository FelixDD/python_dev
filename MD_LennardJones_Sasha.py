# ---------- Méthode de dynamique Moléculaire selon le livre de thermo (p. 466 - 467 ----------

# Librairies utilisées
import random

# Paramètres de la simulation
sigma = 1           # Ordre de grandeur de distance
epsilon = 1         # Ordre de grandeur de l'énergie
m = 1               # Masse d'un atome
rc = 2.5            # Cutoff de calcul du potentiel
N = 216             # Nombre d'atomes
dt = 0.001          # Pas de temps
nb_dt = 2000        # Nombre de pas de temps au cours de la simulation
V = N/0.8           # Volume du cube
rho = N/V           # Densité dans le cube
L = (N/rho)**(1/3)  # Longueur d'un côté du cube


# ---------- FONCTIONS UTILIÉES POUR LA SIMULATION ----------

# Fonction qui calcule la distance r entre deux atomes i et j
def r_ij(ri, rj):
    norme = ((ri[0] - rj[0])**2 + (ri[1] - rj[1])**2 + (ri[2] - rj[2])**2)**(1/2)
    return norme


# Fonction qui calcule l'énergie potentielle de Lennard-Jones entre deux atomes
def u_lj(r):
    energie = 4*epsilon*((r/sigma)**(-12) - (r/sigma)**(-6))
    return energie


# Fonction qui calcule la force sur l'atome i causée par l'atome j
def force_ij(xi, yi, zi, xj, yj, zj, rij):
    f_xi = -(((xi - xj)/rij)*epsilon*(24*(sigma**(-6))*(rij**(-7)) - 48*(sigma**(-12))*(rij**(-13))))
    f_yi = -(((yi - yj)/rij)*epsilon*(24*(sigma**(-6))*(rij**(-7)) - 48*(sigma**(-12))*(rij**(-13))))
    f_zi = -(((zi - zj)/rij)*epsilon*(24*(sigma**(-6))*(rij**(-7)) - 48*(sigma**(-12))*(rij**(-13))))
    return [f_xi, f_yi, f_zi]


# Fonction qui calcule l'énergie potentielle de la configuration et qui donne la liste de force sur chaque atome
def pot_force(r):
    # Inisialisation de l'énergie potentielle
    u = 0
    # Réinisialisation des forces subies par chaque atome
    f = []
    for i in range(N):
        f.append([0.0, 0.0, 0.0])
    # Calcul de l'énergie de chaque paire et de la force sur chaque atome
    for i in range(N - 1):
        for j in range(i + 1, N):
            # Position des deux atomes
            [xi, yi, zi] = [r[i][0], r[i][1], r[i][2]]
            [xj, yj, zj] = [r[j][0], r[j][1], r[j][2]]
            # Distance entre les deux atomes
            rij = r_ij([xi, yi, zi], [xj, yj, zj])
            # Énergie potentielle entre les deux atomes
            u = u + u_lj(rij)
            # Calcul de la force sur l'atome i causée par l'atome j
            [f_xij, f_yij, f_zij] = force_ij(xi, yi, zi, xj, yj, zj, rij)
            # Ajout de la force sur l'atome i causée par l'atome j
            f[i][0] = f[i][0] + f_xij
            f[i][1] = f[i][1] + f_yij
            f[i][2] = f[i][2] + f_zij
            # Ajout de la force sur l'atome j causée par l'atome i
            f[j][0] = f[j][0] - f_xij
            f[j][1] = f[j][1] - f_yij
            f[j][2] = f[j][2] - f_zij
    return [f, u]


# Fonction qui donne les liste des prochaines positions et vitesses (au temps t + dt) selon l'algorithme Verlet
# Voir p. 451 du livre de thermo
def next_r_v(r, v):
    # Calcul des forces qui agissent sur chaque atome
    [f, u] = pot_force(r)
    # Calcul des nouvelles positions
    for i in range(N):
        for j in range(3):
            r[i][j] = r[i][j] + v[i][j]*dt + (f[i][j]/(2*m))*dt**2
    # Calcul partiel des nouvelles vitesses selon les forces actuelles
    for i in range(N):
        for j in range(3):
            v[i][j] = v[i][j] + (f[i][j]/(2*m))*dt
    # Calcul des nouvelles forces selon les nouvelles positions
    [nouv_f, u] = pot_force(r)
    # Finition du calcul des nouvelles vitesses
    for i in range(N):
        for j in range(3):
            v[i][j] = v[i][j] + (nouv_f[i][j]/(2*m))*dt
    return [r, v, u]


# ---------- DÉBUT DE LA SIMULATION ----------

# Liste de la position de chaque atome
# Chaque élément de la liste est une liste de 3 nombres correspondants à la position (x,y,z) de cet élément (atome)
# La position de chaque atome est mise à (0,0,0) pour l'instant
R = []
for i in range(N):
    R.append([0.0, 0.0, 0.0])
# Implémentation des positions initiales des atomes - complètement aléatoire dans le cube
for Rn in R:
    for i in range(3):
        Rn[i] = random.uniform(L/2, L/2)