import numpy as np
import random
import matplotlib.pyplot as plt
#On tient compte que le système sera dans un volume cubique de longueur L. Le facteur de séparation
#sert à séparer les particules à l'initialisation
def initialiser_positions(L, nombre_particules, facteur_separation, dimension):
    position = np.zeros(3 * nombre_particules)
    position = position.reshape(3, nombre_particules)
    position[0][0] = random.uniform(-L, L)
    position[1][0] = random.uniform(-L, L)
    position[2][0] = random.uniform(-L, L)
    i = 1
    condition_sortie = 0
    # Initiation des positions des particules
    while i < nombre_particules:
        # Initialisation des positions pour une particule
        position[0][i] = random.uniform(-L, L)
        position[1][i] = random.uniform(-L, L)
        position[2][i] = random.uniform(-L, L)
        # Vérifier que les particules ne sont pas situées sur les mêmes cases
        if dimension == 2:
            for j in range(0, i):
                if (position[0][i] + facteur_separation <= position[0][j] or position[0][j] <= position[0][
                    i] - facteur_separation
                        or position[1][i] + facteur_separation <= position[1][j] or position[1][j] <= position[1][
                            i] - facteur_separation):
                    condition_sortie = condition_sortie + 1
            if condition_sortie == i:
                i = i + 1
            condition_sortie = 0
        if dimension == 3:
            for j in range(0, i):
                if (position[0][i] + facteur_separation <= position[0][j] or position[0][j] <= position[0][
                    i] - facteur_separation
                        or position[1][i] + facteur_separation <= position[1][j] or position[1][j] <= position[1][
                            i] - facteur_separation):
                    condition_sortie = condition_sortie + 1
            if condition_sortie == i:
                i = i + 1
            condition_sortie = 0
    return position
# Donne le potentiel Lennard-Jones pour une particule
# particule est le i eme particule du systeme
def pot_lj_particule(A, B, position, particule):
    potentiel_tot = 0
    pot_lennard_jones = np.zeros(len(position))
    for i in range(0, len(position) - 1):
        if i != particule:
            rayon = np.sqrt((position[0][i]-position[0][particule])**2+(position[1][i]-position[1][particule])**2)
            pot_lennard_jones[i] = -A/(rayon**6)+B/(rayon**12)
    for j in range(0, len(pot_lennard_jones) - 1):
        potentiel_tot = potentiel_tot + pot_lennard_jones[j]
    return potentiel_tot
def pot_periodique(pos_initial, pos_step, L):
    for i in range(0, len(pos_step[0]) - 1):
        if pos_step[0][i] < -L/2:
            pos_step[0][i] = L/2
        if pos_step[0][i] > L/2:
            pos_step[0][i] = -L/2


# Dit si le déplacement est viable ou pas. À noter que ce code est juste pour un MC en 2D
# Position: un 1D array, particule: integer disant quelle molécule,
# pot_lj float disant le potentiel Lennard_Jones à l'instant présent, pas_deplacement un float
# disant le pas de déplacement, T integer disant la température. Retourne nouveau vecteur de position
def deplacement_accepter(position, particule, pot_lj, pas_deplacement, T):
    # Initialiser les nouvelles positions et les potentiels associés de la particule
    # Tient compte de déplacement négatif et positif
    nouvelle_position_x_pos = position[0][particule] + pas_deplacement
    nouvelle_position_x_neg = position[0][particule] - pas_deplacement
    nouvelle_position_y_pos = position[1][particule] + pas_deplacement
    nouvelle_position_y_neg = position[1][particule] - pas_deplacement
    pot_lj_nouveau_x_pos = 0
    pot_lj_nouveau_x_neg = 0
    pot_lj_nouveau_y_pos = 0
    pot_lj_nouveau_y_neg = 0
    # Vérifier si les potentiels après déplacements sont plus petits
    for i in range(0, len(position[0]) - 1):
        if i != particule:
            pot_lj_nouveau_x_pos = pot_lj_nouveau_x_pos + np.sqrt((position[0][i] - nouvelle_position_x_pos)**2 + (position[1][i] - position[1][particule])**2)
            pot_lj_nouveau_x_neg = pot_lj_nouveau_x_neg + np.sqrt((position[0][i] - nouvelle_position_x_neg)**2 + (position[1][i] - position[1][particule])**2)
            pot_lj_nouveau_y_pos = pot_lj_nouveau_y_pos + np.sqrt((position[0][i] - nouvelle_position_y_pos)**2 + (position[1][i] - position[1][particule])**2)
            pot_lj_nouveau_y_neg = pot_lj_nouveau_y_neg + np.sqrt((position[0][i] - nouvelle_position_y_neg)**2 + (position[1][i] - position[1][particule])**2)
    # On obtient un vecteur des potentiels possibles après un déplacement et les positions possibles après un déplacement
    # (gauche, droite, haut, bas)
    nouveau_vect_pot = [pot_lj_nouveau_x_pos, pot_lj_nouveau_x_neg, pot_lj_nouveau_y_pos, pot_lj_nouveau_y_neg]
    nouveau_vect_pos = [nouvelle_position_x_pos, nouvelle_position_x_neg, nouvelle_position_y_pos, nouvelle_position_y_neg]
    min_nouveau_pot = np.amin(nouveau_vect_pot)
    # i devient la position dans les vecteurs du minimum entre le potentiel des déplacements possibles
    i = 0
    sortie = 0
    while sortie == 0:
        if nouveau_vect_pot[i] == min_nouveau_pot:
            sortie = 1
        i = i+1
    # Mise à jour des  positions
    if min_nouveau_pot < pot_lj:
        if i == 0 or i == 1:
            position[0][particule] = nouveau_vect_pos[i]
        if i == 2 or i == 3:
            position[1][particule] = nouveau_vect_pos[i]
    # Si le potentiel après un step est plus grand que le potentiel actuel
    valeur_comparaison = np.random.sample()
    k = 1.38064852*10**-23
    dU = min_nouveau_pot - pot_lj
    if min_nouveau_pot >= pot_lj:
        if np.exp(-1/(k*T)) < valeur_comparaison:
            if i == 0 or i == 1:
                 position[0][particule] = nouveau_vect_pos[i]
            if i == 2 or i == 3:
                 position[1][particule] = nouveau_vect_pos[i]
        if np.exp(-dU/(k*T)) > valeur_comparaison:
            if i == 0 or i == 1:
                 position[0][particule] = position[0][particule]
            if i == 2 or i == 3:
                 position[1][particule] = position[1][particule]
    return position, min_nouveau_pot
position = initialiser_positions(5, 100, 0.3, 2)
potentiel = pot_lj_particule(1, 1, position, 30)
for j in range(0, 1000):
    for i in range(0, len(position)):
        nouveau_pos, nouveau_pot = deplacement_accepter(position, i, potentiel, 0.3, 273)
        position = nouveau_pos
        potentiel = nouveau_pot
plt.scatter(position[0], position[1])
plt.show()


print(potentiel, nouveau_pot)