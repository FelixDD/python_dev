import numpy as np
import random
import matplotlib.pyplot as plt
#On tient compte que le système sera dans un volume cubique de longueur L. Le facteur de séparation
#sert à séparer les particules à l'initialisation
def initialiser_positions(L, nombre_particules, facteur_separation):
    position = np.zeros(3 * nombre_particules)
    position = position.reshape(3, nombre_particules)
    position[0][0] = random.uniform(-L, L)
    position[1][0] = random.uniform(-L, L)
    # Ajouter une 3e dimension: position[2][0] = random.uniform(-L, L)
    i = 1
    condition_sortie = 0
    # Initiation des positions des particules
    while i < nombre_particules:
        # Initialisation des positions pour une particule
        position[0][i] = random.uniform(-L, L)
        position[1][i] = random.uniform(-L, L)
        # position[2][i] = random.uniform(-L, L)
        # Vérifier que les particules ne sont pas situées sur les mêmes cases
        for j in range(0, i):
            if (position[0][i] + facteur_separation <= position[0][j]  or position[0][j]<= position[0][i] - facteur_separation
                    or position[1][i] + facteur_separation <= position[1][j] or position[1][j] <= position[1][i] - facteur_separation
                    # or position[2][j] != position[2][i]
            ):
                condition_sortie = condition_sortie + 1
        if condition_sortie == i:
            i = i + 1
        condition_sortie = 0
    return position
position = initialiser_positions(5, 100, 0.3)
plt.scatter(position[0], position[1])
plt.show()