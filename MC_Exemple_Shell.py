# - - - - - - - - - - SIMULATION MONTECARLO POUR DES PARTICULES SOUS POTENTIEL LENNARD-JONES EN 2D - Selon le Shell p.468 - - - - - - - - - -

# 00 - Librairies utilisées - - - - - - - - - -

import random
import math
import matplotlib.pyplot as plt
from celluloid import Camera


# 01 - Paramètres physiques et de la simulation - - - - - - - - - -

s = 1                   # Ordre de grandeur de distance
e = 1                   # Ordre de grandeur de l'énergie
m = 1                   # Masse d'un atome
N = 50                  # Nombre d'atomes
T = 2                   # Température
rho = 0.9               # Densité des atomes dans le carré
A = N / rho             # Aire du carré
L = A ** (1/2)          # Longueur des côtés du carré
rc = L / 2              # Distance maximale de calcul de potentiel - Nécessaire pour les conditions frontières périodiques
dr_max = 0.1 * L        # Déplacement maximale dans une coordonnée - À choisir pour atteindre un taux d'acceptation d'environ 50%

nb_it = 25000           # Nombre d'itération pour la simulation


# 02 - Fonctions utilisées dans la simulation - - - - - - - - - -

def r_ij(ri, rj):
    # Fonction qui calcule la distance r entre deux atomes i et j
    norme = ((ri[0] - rj[0]) ** 2 + (ri[1] - rj[1]) ** 2) ** (1 / 2)
    return norme

def u_ij(dist):
    # Fonction qui calcule l'énergie potentielle de Lennard-Jones entre deux atomes séparés d'une distance dist
    u = 4 * e * ( (dist / s)**(-12) - (dist / s)**(-6) )
    return u

def u_tot(r):
    # Fonction qui calcule l'énergie potentielle totale pour une configuration
    # r est une liste de N éléments qui contiennent chacuns deux éléments soient la position en x et en y de chaque atome
    U_totale = 0
    for i in range(N -1):
        for j in range(i + 1, N):
            # Calcul de la distance entre l'atome i et l'atome j
            dist = r_ij(r[i], r[j])
            # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
            if dist <= rc:
                # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                U_totale += u_ij(dist)

        # Calcul des potentiels en lien avec les atomes dans les "carrés extérieurs"
        # Carré à DROITE
        if r[i][0] > 0:
            for j in range(N):
                # Calcul de la distance entre l'atome i et l'atome j qui est dans le carré à droite
                pos_atome_j_droite = [r[j][0] + L, r[j][1]]
                dist = r_ij(r[i], pos_atome_j_droite)
                # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
                if dist <= rc:
                    # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                    U_totale += u_ij(dist)
        # Carré à GAUCHE
        elif r[i][0] < 0:
            for j in range(N):
                # Calcul de la distance entre l'atome i et l'atome j qui est dans le carré à gauche
                pos_atome_j_gauche = [r[j][0] - L, r[j][1]]
                dist = r_ij(r[i], pos_atome_j_gauche)
                # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
                if dist <= rc:
                    # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                    U_totale += u_ij(dist)
        # Carré en HAUT
        elif r[i][1] > 0:
            for j in range(N):
                # Calcul de la distance entre l'atome i et l'atome j qui est dans le carré en haut
                pos_atome_j_haut = [r[j][0], r[j][1] + L]
                dist = r_ij(r[i], pos_atome_j_haut)
                # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
                if dist <= rc:
                    # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                    U_totale += u_ij(dist)
        # Carré en BAS
        elif r[i][1] < 0:
            for j in range(N):
                # Calcul de la distance entre l'atome i et l'atome j qui est dans le carré en bas
                pos_atome_j_bas = [r[j][0], r[j][1] - L]
                dist = r_ij(r[i], pos_atome_j_bas)
                # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
                if dist <= rc:
                    # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                    U_totale += u_ij(dist)
    return U_totale

def u_tot_a(r, a):
    # Fonction qui calcule l'énergie potentielle de Lennard-Jones d'un antome avec tous les autres
    # r est le vecteur qui donne la position de tous les atomes
    # a est un nombre compris dans [1, N] qui dit de quel atome on parle
    U_totale_a = 0
    for i in range(N):
        if i == a:
            pass
        else:
            # Calcul de la distance entre a et i
            dist = r_ij(r[a], r[i])
            # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
            if dist <= rc:
                # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                U_totale_a += u_ij(dist)
    # Calcul des potentiels en lien avec les atomes dans les "carrés extérieurs"
    # Carré à DROITE
    if r[a][0] > 0:
        for j in range(N):
            # Calcul de la distance entre l'atome a et l'atome j qui est dans le carré à droite
            pos_atome_j_droite = [r[j][0] + L, r[j][1]]
            dist = r_ij(r[a], pos_atome_j_droite)
            # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
            if dist <= rc:
                # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                U_totale_a += u_ij(dist)
    # Carré à GAUCHE
    elif r[a][0] < 0:
        for j in range(N):
            # Calcul de la distance entre l'atome a et l'atome j qui est dans le carré à gauche
            pos_atome_j_gauche = [r[j][0] - L, r[j][1]]
            dist = r_ij(r[a], pos_atome_j_gauche)
            # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
            if dist <= rc:
                # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                U_totale_a += u_ij(dist)
    # Carré en HAUT
    elif r[a][1] > 0:
        for j in range(N):
            # Calcul de la distance entre l'atome a et l'atome j qui est dans le carré en haut
            pos_atome_j_haut = [r[j][0], r[j][1] + L]
            dist = r_ij(r[a], pos_atome_j_haut)
            # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
            if dist <= rc:
                # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                U_totale_a += u_ij(dist)
    # Carré en BAS
    elif r[a][1] < 0:
        for j in range(N):
            # Calcul de la distance entre l'atome i et l'atome j qui est dans le carré en bas
            pos_atome_j_bas = [r[j][0], r[j][1] - L]
            dist = r_ij(r[a], pos_atome_j_bas)
            # Vérification que les atomes sont bien séparés d'une distance plus petite que rc
            if dist <= rc:
                # Calcul de l'énergie potentielle de Lennard-Jones entre l'atome i et l'atome j et ajout à U_totale
                U_totale_a += u_ij(dist)
    return U_totale_a

def dep_aleatoire(r):
    # Fonction qui fait varier r, la postion (x, y) d'un atome, en x et en y d'une distance aléatoire maximale dr_max
    r[0] += random.uniform(-1, 1) * dr_max
    if r[0] > L/2:
        r[0] += - L
    elif r[0] < -L/2:
        r[0] += L
    r[1] += random.uniform(-1, 1) * dr_max
    if r[1] > L/2:
        r[1] += - L
    elif r[1] < -L/2:
        r[1] += L
    return r


# 03 - Mise en place de la simulation - - - - - - - - - -

# Initialisation des conditions initiales (positions (x, y) de chaque atome)
# Création de la liste R contenant N éléments de deux floats chacun, étant la position (x, y) de chaque atome
R = []
for i in range(N):
    R.append([random.uniform(-L/2, L/2), random.uniform(-L/2, L/2)])
# Création de la liste U qui contiendra l'énergie potentielle totale de chaque itération
U = []
U.append(u_tot(R))

# Séquence d'itération (simulation totale)
# Création d'une variable qui enregistre le nombre de déplacements qui ont été acceptés afin de calculer le taux d'acceptation
it_acc = 0
# Création de la figure qui servira d'animation
fig_anim = plt.figure()
camera = Camera(fig_anim)
for iteration in range(nb_it):
    print(iteration)
    # Choix d'un atome parmis les N atomes
    atome_choisi = random.randint(0, N-1)
    # Sauvegarde de la position et contribution à l'énergie potentielle totale actuelle de l'atome (avant changement)
    pos_initiale = R[atome_choisi]
    u_a_initiale = u_tot_a(R, atome_choisi)
    # Sauvegarde de l'énergie potentielle totale du système actuelle (avant changement)
    u_tot_initiale = u_tot(R)
    # Déplacement aléatoire de l'atome choisi et création d'une nouvelle liste de position contenant ce changement
    pos_finale = dep_aleatoire(pos_initiale)
    R_final = R
    R_final[atome_choisi] = pos_finale
    # Calcul de la nouvelle contribution à l'énergie potentielle totale de l'atome et de se différence par rapport à sa précédente valeur
    u_a_finale = u_tot_a(R_final, atome_choisi)
    delta_u_a = u_a_finale - u_a_initiale
    # Choix d'accepter ou non le déplacement
    if delta_u_a < 0:
        # Le déplacement est accepté automatiquement
        R = R_final
        it_acc +=1
    elif delta_u_a > 0:
        # Calcul de la probabilité d'accepter le déplacement
        prob_acc = math.exp(- delta_u_a / T)
        # Choix aléatoire
        choix = random.uniform(0, 1)
        if prob_acc > choix:
            # Le déplacement est accepté
            R = R_final
            it_acc += 1
        elif prob_acc < choix:
            # Le déplacement est rejeté
            pass
    # Calcul de la (possiblement) nouvelle énergie potentielle totale du système
    U.append(u_tot(R))
    # À la fin, la liste U aura nb_it + 1 éléments, car on a mis la première énergie potentielle totale
    # Actualisation du graphique
    # Création des données en x et y pour le graphique (les positions en x et y de chaque atome)
    positions_x = []
    positions_y = []
    for atome in R:
        positions_x.append(atome[0])
        positions_y.append(atome[1])
    plt.scatter(positions_x, positions_y, s=3, c="black")
    camera.snap()
# Application de l'animation
animation = camera.animate()
plt.title("Position de chaque atome pour chaque itération")
plt.xlabel("x")
plt.ylabel("y")
plt.show(animation)


# 04 - Résultats graphiques - - - - - - - - - -

# Calcul du taux d'acceptation
taux_acc = round((it_acc / nb_it) * 100)
print("Le taux d'acceptation est de : ", taux_acc, " %.")

# Liste des nb_it + 1 itérations
iteration = []
for i in range(nb_it + 1):
    iteration.append(i)

# Graphique énergie potentielle totale en fonction des itérations
fig_U = plt.figure()
plt.plot(iteration, U)
plt.title("Énergie potentielle totale à chaque itération")
plt.xlabel("Itération")
plt.ylabel("Énergie potentielle totale")
plt.show()