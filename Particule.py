import numpy as np
import matplotlib.pyplot as plt
class Particule(object):
    position_x = 0
    position_y = 0
    vitesse_x = 0
    vitesse_y = 0
    a = 0
    b = 0
    def __init__(self, position_x, position_y, vitesse_x, vitesse_y, a, b):
        self.position_x = position_x
        self.position_y = position_y
        self.vitesse_x = vitesse_x
        self.vitesse_y = vitesse_y
        self.a = a
        self.b = b

def creer_particule(position_x, position_y, vitesse_x, vitesse_y, a, b):
    particule = Particule(position_x, position_y, vitesse_x, vitesse_y, a, b)
    return particule