# Test pour méthode de Monte-Carlo avec un potentiel simple de Lennard-Jones
# Version 1 : Potentiel nul (mouvement brownien) en 1D
import random as rdm
import numpy as np
import math as mth
from celluloid import Camera
import matplotlib.pyplot as plt
# Help
# Nbr d'itérations
nbr = 4000
# Nbr de déplacements acceptés
nbr_accepte = 0
# Nbr de particules
N = 500
# Densité globale (particules/nm^2)
p = 0.8
# Déplacement maximal (nm)
# Sélectionner une valeur pour avoir 30-50% d'acceptance rate (SHELL)
dr = 0.08
# Dimensions de la zone de simulation (nm)
L = (N/p)**(1/2)
# Rayon maximal d'interaction des particules (nm)
cutoff = L/2

# Constante de Boltzmann (J*K)
kb = 1.38*10**(-23)
# Permittivité du vide (F/m)
eo = 8.85418782*10**(-12)
# Température (Kelvins)
T = 3000
# Constantes de Lennard-Jones
A = 2.61*10**(-5)   # (J nm^6 mol^-1)
B = 2.63*10**(-8)   # (J nm^12 mol^-1)
# Charge des particules (C)
q = 0*1.602*10**(-19)

# Évolution de la pression
pression = np.zeros(nbr)
pression[0] = N*kb*T/(L**2) # manque un terme!
# Évolution du potentiel
suivi_potentiel = np.zeros(nbr)

# Initialisation des positions
positionx = np.zeros(N)
positiony = np.zeros(N)
positionz = np.zeros(N)

for i in range(0, N):
    positionx[i] = rdm.random()*L
    positiony[i] = rdm.random() * L

# Initialisation animation
fig = plt.figure()
camera = Camera(fig)

def Lennard_Jones(rayon):
    dU = -A/(rayon**6)+B/(rayon**12)
    return dU

def coulombien(rayon):
    dU = q*q/(4*np.pi*eo*rayon)
    return dU

def calcul_var_pression(rayon_ini, rayon_fin):
    "Calcule la variation de la pression du système lors d'une itération (contribution d'une paire de particule)"
    force_ini = q*q/(4*np.pi*eo*rayon_ini**2) - 6*A/(rayon_ini**7) + 12*B/(rayon_ini**13)
    force_fin = q * q / (4 * np.pi * eo * rayon_fin ** 2)- 6*A/(rayon_fin**7) + 12*B/(rayon_fin**13)
    variation = force_fin*rayon_fin-force_ini*rayon_ini
    return variation


def calcul_potentiel(particule, newpositionx, newpositiony, newpositionz):
    contribution_potentiel = np.zeros(N)
    contribution_pression = np.zeros(N)
    for j in range(0, N):
        if j == particule:
            contribution_potentiel[j] = 0
            contribution_pression[j] = 0
        else:
            diffx = (positionx[j]-positionx[particule])
            diffy = (positiony[j]-positiony[particule])
            diffz = (positionz[j]-positionz[particule])
            if diffx > cutoff:
                diffx = 10 - abs(diffx)
            if diffy > cutoff:
                diffy = 10 - abs(diffy)
            if diffz > cutoff:
                diffz = 10 - abs(diffz)
            rayon_ini = (diffx**2 + diffy**2 + diffz**2)**(1/2)
            newdiffx = (positionx[j] - newpositionx)
            newdiffy = (positiony[j] - newpositiony)
            newdiffz = (positionz[j] - newpositionz)
            if newdiffx > cutoff:
                newdiffx = 10 - abs(newdiffx)
            if newdiffy > cutoff:
                newdiffy = 10 - abs(newdiffy)
            if newdiffz > cutoff:
                newdiffz = 10 - abs(newdiffz)
            rayon_fin = (newdiffx ** 2 + newdiffy ** 2 + newdiffz ** 2) ** (1 / 2)
            contribution_pression[j] = calcul_var_pression(rayon_ini, rayon_fin)
            contribution_potentiel[j] = Lennard_Jones(rayon_fin) + coulombien(rayon_fin) - Lennard_Jones(rayon_ini) - coulombien(rayon_ini)
    dU = sum(contribution_potentiel)
    dP = sum(contribution_pression)
    variation = [dU, dP]
    return variation


def ismoving(particule, dimension, potentiel, index):
    "Vérifie si une particule se déplacera, et assigne dans le cas échéant sa nouvelle position"
    if dimension == 1:
        newpositionx = positionx[particule]+(rdm.random()*2*dr-dr)
        newpositiony = 0
        newpositionz = 0
    elif dimension == 2:
        newpositionx = positionx[particule] + (rdm.random() * 2 * dr - dr)
        newpositiony = positiony[particule] + (rdm.random() * 2 * dr - dr)
        newpositionz = 0
    elif dimension == 3:
        newpositionx = positionx[particule] + (rdm.random() * 2 * dr - dr)
        newpositiony = positiony[particule] + (rdm.random() * 2 * dr - dr)
        newpositionz = positionz[particule] + (rdm.random() * 2 * dr - dr)
    else:
        print("Le nombre de dimensions n'est pas bon.")
        return

    if newpositionx > L:
        newpositionx = newpositionx - L
    elif newpositionx < 0:
        newpositionx = newpositionx + L

    if newpositiony > L:
        newpositiony = newpositiony - L
    elif newpositiony < 0:
        newpositiony = newpositiony + L

    if newpositionz > L:
        newpositionz = newpositionz - L
    elif newpositionz < 0:
        newpositionz = newpositionz + L

    # trigger = np.exp(-dU/(kb*T))
    if potentiel == "brownien":
        dU = 0
        dP = 0
    else:
        variation = calcul_potentiel(particule, newpositionx, newpositiony, newpositionz)
        dU = variation[0]
        dP = variation[1]

    global nbr_accepte
    global suivi_potentiel
    if dU < 0:
        #caca
        nbr_accepte = nbr_accepte+1
        suivi_potentiel[index] = suivi_potentiel[index-1] + dU
        pression[index] = pression[index-1] + dP
        # print(suivi_potentiel[index])
        return newpositionx, newpositiony, newpositionz
    elif np.exp(-dU/(kb*T)) > rdm.random():
        nbr_accepte = nbr_accepte + 1
        suivi_potentiel[index] = suivi_potentiel[index - 1] + dU
        pression[index] = pression[index - 1] + dP
        print(suivi_potentiel[index])
        return newpositionx, newpositiony, newpositionz
    else:
        suivi_potentiel[index] = suivi_potentiel[index - 1]
        pression[index] = pression[index - 1]
        return positionx[particule], positiony[particule], positionz[particule]


def calcul_densite(positionx, positiony,dimension):
    "Calcul la densité locale des particules"
    # grandeur du cube pour le calcul (nm)
    l = L/10
    densite = np.zeros((10, 10))
    for n in range(0, 10):
        for m in range(0, 10):
            # nombre particules dans le cube
            nbr_particule = 0
            for a in range(0,N):
                if (positionx[a] >= (m*l)) & (positionx[a] < (m+1)*l) & (positiony[a] >= (n*l)) & (positiony[a] < (n+1)*l):
                    nbr_particule = nbr_particule + 1
            densite[m, n] = nbr_particule/(l**dimension)
    return densite


def calcul_potentiel_tot():
    contribution = np.zeros((N,N))
    sous_tot = np.zeros(N)
    for particule in range(0,N):
        # range va jusqu'à particule+1 pour ne pas compter en double l'énergie (réduit le temps de calcul)
        for j in range(0, particule+1):
            if j == particule:
                contribution[j] = 0
            else:
                diffx = (positionx[j]-positionx[particule])
                diffy = (positiony[j]-positiony[particule])
                diffz = (positionz[j]-positionz[particule])
                if diffx > cutoff:
                    diffx = 10 - abs(diffx)
                if diffy > cutoff:
                    diffy = 10 - abs(diffy)
                if diffz > cutoff:
                    diffz = 10 - abs(diffz)
                rayon = (diffx**2 + diffy**2 + diffz**2)**(1/2)
                contribution[particule, j] = Lennard_Jones(rayon) + coulombien(rayon)
        sous_tot[particule] = sum(contribution[particule])
    U_tot = sum(sous_tot)
    return U_tot


t = np.zeros(nbr)
# Calcul de la densité initiale (pour fin de comparaison)
densite_ini = calcul_densite(positionx, positiony, 2)

for i in range(0, nbr):
    particule = rdm.randint(0, N-1)
    positionx[particule], positiony[particule], positionz[particule] = ismoving(particule, 2, "Lennard-Jones", i)
    t[i] = i
    # print(positionx)
    plt.scatter(positionx, positiony)
    camera.snap()


# Calcul de la densité finale
densite_fin = calcul_densite(positionx, positiony, 2)

# Rendering de l'animation
animation = camera.animate()
plt.title("Évolution de la simulation")
plt.show(animation)

# Rendering du résultat final uniquement
plt.scatter(positionx, positiony)
plt.title("Résultat final")
plt.show()

ratio = nbr_accepte/nbr
print(ratio)

plt.plot(t, suivi_potentiel)
plt.title("Évolution de l'énergie potentielle")
plt.show()

plt.plot(t, pression)
plt.title("Évolution de la pression")
plt.show()
